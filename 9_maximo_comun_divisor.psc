Algoritmo maximo_comun_divisor
	//diagrama de flujo que calcula el maximo comun divisor de dos numeros enteros positivos
	//N y M siguiendo el algoritmo de euclides, que es el siguiente:
	//a)se divide N por M, sea R es resto.
	//b)si R=0, el maximo comun divisor es M y se acaba.
	//c)se asigna a N el valor de M y a M el valor de R y vuelve al paso a).
	
	Escribir 'ingrese N' 
	Leer N
	Escribir 'ingrese M' 
	Leer M
	Repetir
		R = N mod M
		N = M
		M = R
		escribir R
	Hasta Que R=0
	Escribir M
FinAlgoritmo

